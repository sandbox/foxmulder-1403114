<?php
/**
 * Theme preporocess for simple gmap field view style.
 */
function template_preprocess_views_view_simple_gmap_field_map(&$vars) {

  $rows = array();
  $field_names = array();
  foreach ($vars['rows'] as $num => $row) {
    foreach ($row as $name => $value) {
      $camel_name = simple_gmap_field_camelize($name);
      $rows[$num][$camel_name] = $value;
      if (!in_array($camel_name, $field_names, 1)) {
        if ($vars['options']['lat_long_visible'] == 0 && ($name == 'field_simple_gmap_latitude' || $name == 'field_simple_gmap_longitude')) {
          continue;
        }
        else {
          array_push($field_names, $camel_name);
        }
      }
    }
  }

  drupal_add_js(array(
    'simpleGmapField' => array(
      'action'             => 'view_view',
      'mapCenterLatitude'  => $vars['options']['mapcenter_latitude'],
      'mapCenterLongitude' => $vars['options']['mapcenter_longitude'],
      'mapZoom'            => $vars['options']['zoom'],
      'rows'               => $rows,
      'fieldNames'         => $field_names
    )), 'setting');
}

function simple_gmap_field_camelize($string) {
  $string = str_replace(array('-', '_'), ' ', $string); 
  $string = ucwords($string); 
  $string = str_replace(' ', '', $string);  
  return lcfirst($string);
}
