<?php
/**
 * $view
 *   The full view object
 *
 * $rows
 *
 * $field_names
 *
 */
drupal_add_js(drupal_get_path('module', 'simple_gmap_field') . '/simple_gmap_field.js', 'file');
?>
<div id="view-attachment-before"><?php print $view->attachment_before; ?></div>
<div id="simple-gmap-field-gmap-canvas" style="width:<?php print $options['width']?>px; height:<?php print $options['height']?>px;"></div>
<div id="view-attachment-after" class="clearfix"><?php print $view->attachment_after; ?></div>

