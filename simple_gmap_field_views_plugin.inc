<?php
// $Id: nicemap_views_plugin.inc,v 1.2.2.13 2009/04/14 17:54:27 jmiccolis Exp $

/**
 * @file
 * Extending the view_plugin_style class to provide a nicemap view style.
 */
class simple_gmap_field_views_plugin extends views_plugin_style {

  function option_definition() {
    $options = parent::option_definition();
    $options['width'] = array('default' => 600);
    $options['height'] = array('default' => 300);
    $options['mapcenter_latitude'] = array('default' => 38.410558);
    $options['mapcenter_longitude'] = array('default' => -28.212891);
    $options['zoom'] = array('default' => 1);
    $options['lat_long_visible'] = array('default' => 0);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    $form['gmap'] = array(
      '#markup' => '<div id="simple-gmap-field-gmap-canvas"></div>'
    );
    drupal_add_js(array(
      'simpleGmapField' => array(
        'action'             => 'edit_view',
        'mapCenterLatitude'  => $this->options['mapcenter_latitude'],
        'mapCenterLongitude' => $this->options['mapcenter_longitude'],
        'mapZoom'            => $this->options['zoom'],
      )), 'setting'
    );
    drupal_add_js(drupal_get_path('module', 'simple_gmap_field') . '/simple_gmap_field.js', 'file');
    $form['width'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Width of the map in pixels'),
      '#default_value' => $this->options['width'],
      '#maxlength'     => 10,
      '#size'          => 10
    );
    $form['height'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Height of the map in pixels'),
      '#default_value' => $this->options['height'],
      '#maxlength'     => 10,
      '#size'          => 10
    );
    $form['zoom'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Zoom of the map'),
      '#default_value' => $this->options['zoom'],
      '#maxlength'     => 3,
      '#size'          => 3
    );
    $form['mapcenter_latitude'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Latitude value of center of the map'),
      '#default_value' => $this->options['mapcenter_latitude'],
      '#maxlength'     => 10,
      '#size'          => 10
    );
    $form['mapcenter_longitude'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Longitude value of center of the map'),
      '#default_value' => $this->options['mapcenter_longitude'],
      '#maxlength'     => 10,
      '#size'          => 10
    );
    $form['lat_long_visible'] = array(
      '#type'          => 'checkbox',
      '#title'         => t('Latitude/Longitude values are visible in infowindow.'),
      '#default_value' => $this->options['lat_long_visible']
    );
  }

  /**
   * views_plugin_style::render_fields() was overwritten, because we need node nid in rendered output
   */
  function render_fields($result) {

    if (!$this->uses_fields()) {
      return;
    }

    if (!isset($this->rendered_fields)) {
      $this->rendered_fields = array();
      $this->view->row_index = 0;
      $keys = array_keys($this->view->field);

      if (!empty($keys)) {
        foreach ($result as $count => $row) {
          $this->view->row_index = $count;
          foreach ($keys as $id) {
            $this->rendered_fields[$count][$id] = $this->view->field[$id]->theme($row);
            // Adding nid to output
            $this->rendered_fields[$count]['nid'] = $row->nid;
          }

          $this->row_tokens[$count] = $this->view->field[$id]->get_render_tokens(array());
        }
      }
      unset($this->view->row_index);
    }

    return $this->rendered_fields;
  }

  function render() {
    return theme($this->theme_functions(), array(
      'view'        => $this->view,
      'options'     => $this->options,
      'rows'        => $this->render_fields($this->view->result),
      'field_names' => array_keys($this->view->field)
    ));
  }

}
