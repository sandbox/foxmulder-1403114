<?php

/**
 * @file
 * Views file for simple_gmap_field module.
 */

/**
 * Implements hook_field_views_data().
 */
function simple_gmap_field_field_views_data() {
  $data = array();
  foreach (field_info_fields() as $field) {
    if ($field['storage']['type'] != 'field_sql_storage') {
      continue;
    }
    $result = array();
    if ($field['module'] == 'simple_gmap_field') {
      $result = field_views_field_default_views_data($field);
      unset($result['field_data_' . $field['field_name']][$field['field_name']]);
      unset($result['field_data_' . $field['field_name']][$field['field_name'] . '_zoom']);
      unset($result['field_revision_' . $field['field_name']][$field['field_name']]);
      unset($result['field_revision_' . $field['field_name']][$field['field_name'] . '_zoom']);
      $result['field_data_' . $field['field_name']][$field['field_name'] . '_latitude']['field'] = array(
        'table'             => 'field_data_' . $field['field_name'],
        'handler'           => 'views_handler_field_numeric',
        'click sortable'    => 1,
        'field_name'        => 'field_simple_gmap_latitude',
        'additional fields' => array('delta', 'language', 'bundle'),
        'entity_tables'     => array('node' => 'node', 'node_revision' => 'node'),
        'element type'      => 'div',
        'is revision'       => 0
      );
      $result['field_data_' . $field['field_name']][$field['field_name'] . '_longitude']['field'] = array(
        'table'             => 'field_data_' . $field['field_name'],
        'handler'           => 'views_handler_field_numeric',
        'click sortable'    => 1,
        'field_name'        => 'field_simple_gmap_longitude',
        'additional fields' => array('delta', 'language', 'bundle'),
        'entity_tables'     => array('node' => 'node', 'node_revision' => 'node'),
        'element type'      => 'div',
        'is revision'       => 0
      );
    }

    if (is_array($result)) {
      $data = drupal_array_merge_deep($result, $data);
    }
  }
  return $data;
}

/**
 * Implements hook_views_style_plugins().
 */
function simple_gmap_field_views_plugins() {
  return array(
    'style' => array(
      'simple_gmap_field' => array(
        'title' => t('Simple Gmap'),
        'help' => t('Displays content on a map.'),
        'handler' => 'simple_gmap_field_views_plugin',
        'theme' => 'views_view_simple_gmap_field_map',
        'theme file' => 'simple_gmap_field_views.theme.inc',
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
        'even empty' => TRUE,
      )
    )
  );
}
