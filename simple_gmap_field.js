/**
 * Simple Gmap field JS
 */
(function ($) {
var map, marker, markers = [], geocoder, infoWindows = [], baseMapCenter;

$().ready(function(){
  baseMapCenter = new google.maps.LatLng(Drupal.settings.simpleGmapField.mapCenterLatitude, Drupal.settings.simpleGmapField.mapCenterLongitude);

  zoom = (typeof(Drupal.settings.simpleGmapField.mapZoom) == 'number') ?
         Drupal.settings.simpleGmapField.mapZoom :
         parseInt(Drupal.settings.simpleGmapField.mapZoom);

  map = new google.maps.Map(document.getElementById("simple-gmap-field-gmap-canvas"), {
    zoom: zoom,
    center: baseMapCenter,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  marker = new google.maps.Marker({
    title: "",
    position: baseMapCenter,
    map: map
  });

  switch (Drupal.settings.simpleGmapField.action) {

    case 'view_node':
      marker.setDraggable(false);
      map.setOptions( {draggable: false} );

      google.maps.event.addListener(map, 'zoom_changed', function() {
        map.setCenter(marker.getPosition());
      });
      break;

    case 'edit_node':
      marker.setDraggable(true);
      google.maps.event.addListener(marker, 'dragend', function(event) {
        var markerPos = marker.getPosition().toUrlValue().split(",");
        $("#edit-field-simple-gmap-und-0-latitude").val(markerPos[0]);
        $("#edit-field-simple-gmap-und-0-longitude").val(markerPos[1]);
        map.setCenter(marker.getPosition());
      });

      $("#edit-field-simple-gmap-und-0-zoom").change(function() {
        map.setZoom(parseInt($("#edit-field-simple-gmap-und-0-zoom").val()));
      });

      $("#edit-field-simple-gmap-und-0-latitude").change(function() {
        var newCenter = new google.maps.LatLng($("#edit-field-simple-gmap-und-0-latitude").val(), marker.getPosition().lng());
        marker.setPosition(newCenter);
        map.setCenter(newCenter);
      });

      $("#edit-field-simple-gmap-und-0-longitude").change(function() {
        var newCenter = new google.maps.LatLng(marker.getPosition().lng(), $("#edit-field-simple-gmap-und-0-latitude").val());
        marker.setPosition(newCenter);
        map.setCenter(newCenter);
      });

      google.maps.event.addListener(map, 'zoom_changed', function() {
        $("#edit-field-simple-gmap-und-0-zoom").val(map.getZoom());
      });

      google.maps.event.addListener(marker, 'dblclick', function(event) {
        $("#edit-field-simple-gmap-und-0-latitude").val(0);
        $("#edit-field-simple-gmap-und-0-longitude").val(0);
        $("#edit-field-simple-gmap-und-0-zoom").val(0);
        marker.setMap(null);
      });
      break;

    case 'edit_view':
      marker.setDraggable(true);
      google.maps.event.addListener(marker, 'dragend', function(event) {
        var markerPos = marker.getPosition().toUrlValue().split(",");
        $("#edit-style-options-mapcenter-latitude").val(markerPos[0]);
        $("#edit-style-options-mapcenter-longitude").val(markerPos[1]);
        map.setCenter(marker.getPosition());
      });

      $("#edit-style-options-zoom").change(function() {
        map.setZoom(parseInt($("#edit-style-options-zoom").val()));
      });

      $("#edit-style-options-mapcenter-latitude").change(function() {
        var newCenter = new google.maps.LatLng($("#edit-style-options-mapcenter-latitude").val(), marker.getPosition().lng());
        marker.setPosition(newCenter);
        map.setCenter(newCenter);
      });
      $("#edit-style-options-mapcenter-longitude").change(function() {
        var newCenter = new google.maps.LatLng(marker.getPosition().lng(), $("#edit-style-options-mapcenter-longitude").val());
        marker.setPosition(newCenter);
        map.setCenter(newCenter);
      });

      google.maps.event.addListener(map, 'zoom_changed', function() {
        $("#edit-style-options-zoom").val(map.getZoom());
      });
      break;

    case 'view_view':
      marker.setMap(null);
      for (var i=0; i<Drupal.settings.simpleGmapField.rows.length; i++) {
        var row = Drupal.settings.simpleGmapField.rows[i];
        if (row.fieldSimpleGmapLatitude && row.fieldSimpleGmapLongitude) {
          markers[row.nid] = new google.maps.Marker({
            position: new google.maps.LatLng(row.fieldSimpleGmapLatitude, row.fieldSimpleGmapLongitude),
            map: map
          });
          var infoWindowContent = '';
          for (var j=0; j<Drupal.settings.simpleGmapField.fieldNames.length; j++) {
            var fieldName = Drupal.settings.simpleGmapField.fieldNames[j];
            if (fieldName != 'nid') {
//alert('nid: '+row[fieldName]);
              infoWindowContent += row[fieldName]+'<br />';
            }
          }
          attachInfoWindow(row.nid, infoWindowContent);
        }
      }
      break;
  }

});

// a nid azonositja a markert es az infoWindow-t
function attachInfoWindow(nid, content) {
  infoWindows[nid] = new google.maps.InfoWindow( {content:content} );
  google.maps.event.addListener(markers[nid], 'click', function() {
    infoWindows[nid].open(map, markers[nid]);
  });
}

function print_r(theObj){
  alertText = '';
  if(theObj.constructor == Array || theObj.constructor == Object){
    for(var p in theObj){
      if(theObj[p].constructor == Array || theObj[p].constructor == Object){
        alertText+="["+p+"] => "+typeof(theObj)+"\n";
        alertText+="-";
        print_r(theObj[p]);
        alertText+="\n";
      } else {
        alertText+="["+p+"] => "+theObj[p]+"</li>";
      }
    }
  }
  alert(alertText);
}
})(jQuery);


