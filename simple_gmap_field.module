<?php
// $Id: $

/**
 * @file
 * Module file for the simple_gmap_field module.
 */

/**
 * Implements hook_help().
 */
function simple_gmap_field_help($path, $arg) {
  if ($path == 'admin/help#simple_gmap_field') {
    return t('Adds a location field and a Views style plugin to display nodes on a Google map.');
  }
}

/**
 * Implements hook_init().
 */
function simple_gmap_field_init() {
  drupal_add_css(drupal_get_path('module', 'simple_gmap_field') . '/simple_gmap_field.css', array('group' => CSS_DEFAULT, 'every_page' => TRUE));
  drupal_add_js('http://maps.googleapis.com/maps/api/js?sensor=false', 'external');
}

/**
 * Implements hook_permission
 */
function simple_gmap_field_permission() {
  return array(
    'admin simple gmap field' => array(
      'title'       => t('Admin simple gmap field'),
      'description' => t('Administer default settings of Simple Gmap field module')
    )
  );
}

/**
 * Implements hook_views_api().
 */
function simple_gmap_field_views_api() {
  return array(
    'api' => '3.0'
  );
}

/**
 * Implements hook_field_info().
 */
function simple_gmap_field_field_info() {
  return array(
    'simple_gmap_field' => array(
      'label'             => t('Simple gmap field'),
      'description'       => t('The field stores latitude and longitude data.'),
      'settings'          => array(
        'latitude'  => 38.410558,
        'longitude' => -28.212891,
        'zoom'      => 1
      ),
      'default_widget'    => 'simple_gmap_field_widget',
      'default_formatter' => 'simple_gmap_field_gmap',
    )
  );
}

/**
 * Implements hook_field_widget_info().
 */
function simple_gmap_field_field_widget_info() {
  return array(
    'simple_gmap_field_widget' => array(
      'label'       => t('Simple gmap field'),
      'field types' => array('simple_gmap_field'),
      'behaviors'   => array(
        // Több koordináta is tartozhat egy node-hoz? Akár...
        'multiple values' => FIELD_BEHAVIOR_DEFAULT, 
        'default value'   => FIELD_BEHAVIOR_DEFAULT,
      ),
    )
  );
}

/**
 * Implements hook_field_widget_form().
 */
function simple_gmap_field_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {          
  $latitude = ''; $longitude = ''; $zoom = '';

  if (!isset($items[$delta]['latitude']) || $items[$delta]['latitude'] == 00.000000) {
    $latitude = isset($instance['default_value'][$delta]['latitude']) ? $instance['default_value'][$delta]['latitude'] : $field['settings']['latitude'];
  }
  else {
    $latitude = $items[$delta]['latitude'];
  }

  if (!isset($items[$delta]['longitude']) || $items[$delta]['longitude'] == 00.000000) {
    $longitude = isset($instance['default_value'][$delta]['longitude']) ? $instance['default_value'][$delta]['longitude'] : $field['settings']['longitude'];
  }
  else {
    $longitude = $items[$delta]['longitude'];
  }

  if (!isset($items[$delta]['zoom']) || $items[$delta]['zoom'] == 0) {
    $zoom = isset($instance['default_value'][$delta]['zoom']) ? $instance['default_value'][$delta]['zoom'] : $field['settings']['zoom'];
  }
  else {
    $zoom = $items[$delta]['zoom'];
  }

  drupal_add_js(array(
    'simpleGmapField' => array(
      'action'             => 'edit_node',
      'mapCenterLatitude'  => $latitude,
      'mapCenterLongitude' => $longitude,
      'mapZoom'            => $zoom,
    )), 'setting');
  drupal_add_js(drupal_get_path('module', 'simple_gmap_field') . '/simple_gmap_field.js', 'file');
  $element += array(
      '#type'  => 'fieldset',
      'simple_gmap_field_canvas' => array(
        '#markup'   => '<div id="simple-gmap-field-gmap-canvas" class="simple-gmap-field-gmap-canvas simple-gmap-field-gmap-canvas-edit"></div>'
      ),
      'latitude' => array(
        '#type'          => 'textfield',
        '#title'         => t('Latitude'),
        '#maxlength'     => 10,
        '#size'          => 10,
        '#default_value' => $latitude,
        '#attributes'    => array('readonly' => TRUE)
      ),
      'longitude' => array(
        '#type'          => 'textfield',
        '#title'         => t('Longitude'),
        '#maxlength'     => 10,
        '#size'          => 10,
        '#default_value' => $longitude,
        '#attributes'    => array('readonly' => TRUE)
      ),
      'zoom' => array(
        '#type'          => 'textfield',
        '#title'         => t('Zoom'),
        '#maxlength'     => 3,
        '#size'          => 3,
        '#default_value' => $zoom,
        '#attributes'    => array('readonly' => TRUE)
      )
  );
  return $element;
}

/**
 * Implements hook_field_is_empty().
 */
function simple_gmap_field_field_is_empty() {
}

/**
 * Implements hook_field_formatter_info().
 */
function simple_gmap_field_field_formatter_info() {
  return array(
    'simple_gmap_field_gmap' => array(
      'label'       => t('Google map'),
      'field types' => array('simple_gmap_field')
    ),
    'simple_gmap_field_latlng' => array(
      'label'       => t('Latitude/Longitude'),
      'field types' => array('simple_gmap_field')
    )
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function simple_gmap_field_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $element = array();

  switch ($display['type']) {
    case 'simple_gmap_field_latlng':
      $element['display_labels'] = array(
        '#title'         => t('Displaying labels'),
        '#description'   => t('Displaying latitude/longitude labels'),
        '#type'          => 'checkbox',
        '#default_value' => isset($settings['display_labels']) ? $settings['display_labels'] : 1
      );
      break;
    case 'simple_gmap_field_gmap':
      $element['separate_maps'] = array(
        '#title'         => t('Separate maps'),
        '#description'   => t('If more than one values was added, the values will be displayed separate maps or on a common one.'),
        '#type'          => 'checkbox',
        '#default_value' => isset($settings['separate_maps']) ? $settings['separate_maps'] : 0
      );
      break;
  }
  return $element;
}

/**
 * Implements hook_field_formatter_prepare_view().
 */
function simple_gmap_field_field_formatter_prepare_view($entity_type, $entities, $field, $instances, $langcode, &$items, $displays) {

}

/**
 * Implements hook_field_formatter_view().
 */
function simple_gmap_field_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {

  $element = array();
  switch ($display['type']) {
    case 'simple_gmap_field_gmap':
      // Több értéke is lehet a mezőnek, de csak egy térkép van (egyelőre csak egy értéket vesz figyelembe)
      $output = '';
      foreach ($items as $delta => $item) {
        // Itt már nincs output .=, csak értékek küldése a JS-nek. De mit küldünk? Latitude és longitude értékeket, zoom viszont csak egy lehet.
        if ($item['latitude'] != 0 && $item['longitude'] != 0 && $item['zoom'] != 0) {
          $element[$delta] = array(
            '#markup' => '<div id="simple-gmap-field-gmap-canvas" class="simple-gmap-field-gmap-canvas simple-gmap-field-gmap-canvas-view"></div>'
          );
          drupal_add_js(array(
            'simpleGmapField' => array(
              'action'             => 'view_node',
              'mapCenterLatitude'  => $item['latitude'],
              'mapCenterLongitude' => $item['longitude'],
              'mapZoom'            => $item['zoom']
            )), 'setting');
        }
      }
      $element['gmap'] = array('#markup' => $output);
      break;
    case 'simple_gmap_field_latlng':
      foreach ($items as $delta => $item) {
        $output = '<div class="simple-gmap-field-latitude">';
        $output .= t('Latitude') . ': ' . $item['latitude'];
        $output .= '</div>';
        $output .= '<div class="simple-gmap-field-longitude">';
        $output .= t('Longitude') . ': ' . $item['longitude'];
        $output .= '</div>';
        $element[$delta] = array('#markup' => $output);
      }
      break;
  }
  drupal_add_js(drupal_get_path('module', 'simple_gmap_field') . '/simple_gmap_field.js', 'file');
  return $element;
}

